-- This is the init file for the Rijschoolvinden webapp

-- This inserts all the default licence types

INSERT INTO `licenses`(`name`, `type`) VALUES
('Auto','praktijk'),
('Auto plus aanhangwagen','praktijk'),
('Brommer','praktijk'),
('Motor','praktijk'),
('Vrachtwagen','praktijk'),
('Aanhanger achter vrachtwagen','praktijk'),
('Autobus','praktijk'),
('Taxi opleiding','praktijk'),
('Auto','theorie'),
('Motor','theorie'),
('Bromfiets','theorie'),
('Code 95','theorie');
