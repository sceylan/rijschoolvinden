<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LeadCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:leads')
            ->setDescription('This command function emails all companies with leads.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $leadService = $this->getContainer()->get('service.lead');
        $leadService->sendLeads();
    }
}

?>