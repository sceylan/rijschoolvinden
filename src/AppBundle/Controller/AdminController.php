<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LicenceCity;
use AppBundle\Entity\User;
use AppBundle\Entity\UserLead;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_RIJSCHOOL')")
 */
class AdminController extends Controller
{

    private function initSystem()
    {


    }

    /**
     * @Route("/admin/dashboard/", name="admin_dashboard")
     * @Template()
     */
    public function dashboardAction(Request $request)
    {

        // remove dublicate code
        $user = $this->getUser();
        $roles = $user->getRoles();
        foreach($roles as $role)
        {
            if($role === 'ROLE_ADMIN')
                return $this->redirectToRoute('adminplus_dashboard');
        }

        if($user->getStatus() === User::STATUS_DEACTIVATED)
            return $this->redirectToRoute('fos_user_security_logout');

        $wizard = $user->getWizardCompleted();
        if($wizard === User::WIZARD_NOT_COMPLETED)
            return $this->redirectToRoute('admin_wizard');

        $complete = $user->getWelcomeCompleted();
        if($complete === User::WELCOME_NOT_COMPLETED)
            return $this->redirectToRoute('admin_welcome');
        // remove dublicate code

        $beginThisMonth = strtotime(date('Y-m-01'));
        $endThisMonth = strtotime(date('Y-m-t'));

        $beginLastMonth = strtotime("+1 month", $beginThisMonth);
        $endLastMonth = strtotime("+1 month", $endThisMonth);

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('COUNT(ul)')
            ->from('AppBundle:UserLead', 'ul')
            ->where('ul.dateTimeEmailed BETWEEN :beginThisMonth AND :endThisMonth')
            ->andWhere('ul.user = :userID')
            ->setParameter('beginThisMonth', $beginThisMonth)
            ->setParameter('endThisMonth', $endThisMonth)
            ->setParameter('userID', $this->getUser()->getId());

        $countThisMonth = $qb->getQuery()->getSingleScalarResult();

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('COUNT(ul)')
            ->from('AppBundle:UserLead', 'ul')
            ->where('ul.dateTimeEmailed BETWEEN :beginLastMonth AND :endLastMonth')
            ->andWhere('ul.user = :userID')
            ->setParameter('beginLastMonth', $beginLastMonth)
            ->setParameter('endLastMonth', $endLastMonth)
            ->setParameter('userID', $this->getUser()->getId());

        $countLastMonth = $qb->getQuery()->getSingleScalarResult();

        $userLeads = $this->getUser()->getLeads();
        $licenceCities = $this->getDoctrine()->getRepository("AppBundle:LicenceCity")->findBy(['user' => $this->getUser()]);

        return array(
            'userLeads' => $userLeads,
            'licenceCities' => $licenceCities,
            'countThisMonth' => $countThisMonth,
            'countLastMonth' => $countLastMonth
            );
    }

    /**
     * @Route("/admin/wizard/", name="admin_wizard")
     * @Template()
     */
    public function wizardAction(Request $request)
    {

        if($request->getMethod() == 'POST')
        {
            $firstname = $request->get('firstname');
            $lastname = $request->get('lastname');
            $phoneNumber = $request->get('phonenumber');
            $companyName = $request->get('companyname');
            $address = $request->get('address');
            $houseNumber = $request->get('housenumber');
            $zipcode = str_replace(' ', '', $request->get('zipcode'));
            $city = $request->get('city');
            $cityID = $request->get('rijschool-city');
            $licenceID = $request->get('rijschool-licence');
//            $rijschoolAmount = $request->get('rijschool-amount');
            $subscriptionType = (int)$request->get('abonnement');

            $error = false;
            $em = $this->getDoctrine()->getManager();
            $validator = $this->get('service.validator');
            $values = [$firstname, $lastname, $phoneNumber, $companyName, $address, $houseNumber, $zipcode, $city];
            $typeError = '';


            foreach($values as $value)
            {
                if(!$validator->validateEmptyValue($value))
                {
                    $error = true;
                }

            }

            if(!$validator->validateZipCode($zipcode))
            {
                $this->addFlash('danger', "U heeft een verkeerd postcode opgegeven in het formulier. Verbeter de fouten en probeer het opnieuw.");
                $error = true;
            }


            if(!$validator->validatePhoneNumber($phoneNumber))
            {
                $this->addFlash('danger', "U heeft een verkeerd telefoonnummer opgegeven in het formulier. Verbeter de fouten en probeer het opnieuw.");
                $error = true;
            }

            $cityObject = $this->getDoctrine()->getRepository('AppBundle:City')->find($cityID);
            if(!$cityObject)
            {
                $this->addFlash('danger', "U heeft een verkeerd stad opgegeven in het formulier. Verbeter de fouten en probeer het opnieuw.");
                $error = true;
            }

            $licenceObject = $this->getDoctrine()->getRepository('AppBundle:Licence')->find($licenceID);
            if(!$licenceObject)
            {
                $this->addFlash('danger', "U heeft een verkeerd rijbewijstype opgegeven in het formulier. Verbeter de fouten en probeer het opnieuw.");
                $error = true;
            }

            if($subscriptionType !== 0 && $subscriptionType !== 1 && $subscriptionType !== 2)
            {
                $this->addFlash('danger', "U heeft geen abonnementsvorm geselecteerd. Verbeter de fouten en probeer het opnieuw.");
                $error = true;
            }

//            if(!$validator->validateNumericValue($rijschoolAmount) && strtolower($rijschoolAmount) !== 'geen')
//            {
//                $this->addFlash('danger', "U heeft een verkeerd aantal limieten opgegeven in het formulier. Verbeter de fouten en probeer het opnieuw.");
//                $error = true;
//            }



            if($error === true)
            {
                $user = $this->getUser();
                $user->setStatus(User::STATUS_INACTIVE);
                $em->flush();
                $this->addFlash('danger', "U heeft een of meerdere fouten in het formulier. Verbeter de fouten en probeer het opnieuw.");
            } else {
                /**
                 * @var $user User
                 */
                $user = $this->getUser();
                $user->setFirstName($firstname);
                $user->setLastName($lastname);
                $user->setPhoneNumber($phoneNumber);
                $user->setCompanyName($companyName);
                $user->setAddress($address);
                $user->setHouseNumber($houseNumber);
                $user->setZipcode($zipcode);
                $user->setCity($city);
                $user->setStatus(User::STATUS_ACTIVATING);
                $user->setWizardCompleted(User::WIZARD_COMPLETED);
                $user->setSubscriptionType($subscriptionType);

                $licenceCity = new LicenceCity();
                $licenceCity->setCity($cityObject);
                $licenceCity->setLicence($licenceObject);
                $licenceCity->setUser($this->getUser());
                $licenceCity->setMaxLimit(null);

//                if($rijschoolAmount === 'geen')
//                {
//                    $licenceCity->setMaxLimit(0);
//                } else {
//
//                    if((int)$rijschoolAmount === 0) {
//                        $licenceCity->setMaxLimit(null);
//                    } else {
//                        $licenceCity->setMaxLimit($rijschoolAmount);
//                    }
//                }

                $em->persist($licenceCity);
                $em->flush();

//                $this->addFlash('success', 'Uw gegevens zijn succesvol geupdate. Hieronder ');

                $message = \Swift_Message::newInstance()
                    ->setSubject('Nieuwe rijschool aanmelding')
                    ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setTo(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setBody(
                        $this->renderView(
                            'AppBundle:Emails:validatie.html.twig',
                            array('companyName' => $companyName,
                                'city' => $city,
                                'firstName' => $firstname,
                                'lastName' => $lastname,
                                'amountLeads' => 0)
                        ),
                        'text/html'
                    )
                ;
                $this->get('mailer')->send($message);

                return $this->redirectToRoute('admin_cities');
            }
        }
        $cities = $this->getDoctrine()->getRepository('AppBundle:City')->findAll();
        $licences = $this->getDoctrine()->getRepository('AppBundle:Licence')->findAll();
        return array('cities' => $cities, 'licenses' => $licences);
    }

    /**
     * @Route("/admin/offerteaanvragen/", name="admin_leads")
     * @Template()
     */
    public function leadsAction(Request $request)
    {
        // remove dublicate code
        $user = $this->getUser();
        $roles = $user->getRoles();
        foreach($roles as $role)
        {
            if($role === 'ROLE_ADMIN')
                return $this->redirectToRoute('adminplus_dashboard');
        }

        if($user->getStatus() === User::STATUS_DEACTIVATED)
            return $this->redirectToRoute('fos_user_security_logout');

        $wizard = $user->getWizardCompleted();
        if($wizard === User::WIZARD_NOT_COMPLETED)
            return $this->redirectToRoute('admin_wizard');

        $complete = $user->getWelcomeCompleted();
        if($complete === User::WELCOME_NOT_COMPLETED)
            return $this->redirectToRoute('admin_welcome');
        // remove dublicate code

        $userLeads = $this->getUser()->getLeads();
        return array('userLeads' => $userLeads);
    }

    /**
     * @Route("/admin/student-offerteaanvraag/{id}", name="admin_student_lead")
     * @Template()
     */
    public function studentLeadAction(Request $request, $id)
    {
        $userLead = $this->getDoctrine()->getRepository('AppBundle:UserLead')->find($id);
        if(!$userLead)
        {
            $this->addFlash('danger', 'Geen lead gevonden. Probeer het opnieuw of neem contact op met de helpdesk.');
            return $this->redirectToRoute('admin_leads');
        }

        $userLead->setStudent(UserLead::STUDENT_YES);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash('success', 'Bedankt voor het doorgeven!');
        return $this->redirectToRoute('admin_lead', ['lead_id' => $userLead->getLead()->getId()]);
    }

    /**
     * @Route("/admin/lead/{lead_id}", name="admin_lead")
     * @Template()
     */
    public function leadAction($lead_id, Request $request)
    {

        // remove dublicate code
        $user = $this->getUser();
        $roles = $user->getRoles();
        foreach($roles as $role)
        {
            if($role === 'ROLE_ADMIN')
                return $this->redirectToRoute('adminplus_dashboard');
        }

        if($user->getStatus() === User::STATUS_DEACTIVATED)
            return $this->redirectToRoute('fos_user_security_logout');

        $wizard = $user->getWizardCompleted();
        if($wizard === User::WIZARD_NOT_COMPLETED)
            return $this->redirectToRoute('admin_wizard');

        $complete = $user->getWelcomeCompleted();
        if($complete === User::WELCOME_NOT_COMPLETED)
            return $this->redirectToRoute('admin_welcome');
        // remove dublicate code

        $validator = $this->get('service.validator');
        if(!$validator->validateNumericValue($lead_id))
            throw new \Exception("lead details page error");

        $lead = $this->getDoctrine()->getRepository('AppBundle:Lead')->find($lead_id);

        if(!$lead)
            throw new \Exception('Lead not found!');

        $userLead = $this->getDoctrine()->getRepository('AppBundle:UserLead')->findOneBy(['lead' => $lead, 'user' => $this->getUser()]);

        if(!$userLead)
        {
            $userLead = false;
        }

        return array('lead' => $lead, 'userLead' => $userLead);
    }

    /**
     * @Route("/admin/lead/rapporteer/{lead_id}", name="admin_lead_report")
     * @Template()
     */
    public function reportAction($lead_id, Request $request)
    {

        $validator = $this->get('service.validator');
        if(!$validator->validateNumericValue($lead_id))
            throw new \Exception("lead details page error");

        /**
         * @var $userLead UserLead
         */
        $userLead = $this->getDoctrine()->getRepository('AppBundle:UserLead')->findOneBy(['id' => $lead_id, 'user' => $this->getUser()]);

        if(!$userLead)
            throw new \Exception('Lead not found!');

        if($userLead->getReported()=== UserLead::STATUS_REPORTED) {
            $this->addFlash('danger', 'Deze lead is al eerder gerapporteerd en is momenteel in behandeling.');
            return $this->redirectToRoute('admin_leads');
        }

        $reason = $request->get('reason');

        $validator = $this->get('service.validator');
        if(!$validator->validateEmptyValue($reason))
        {
            $this->addFlash('danger', 'U heeft geen toelichting opgegeven. U moet uw rapportage toelichten, zodat wij een onderzoek naar kunnen stellen.');
            return $this->redirectToRoute('admin_lead', ['lead_id' => $userLead->getLead()->getId()]);
        }

        $em = $this->getDoctrine()->getManager();
        $userLead->setReported(UserLead::STATUS_REPORTED);
        $userLead->setReportReason($reason);
        $em->flush();

        $message = \Swift_Message::newInstance()
            ->setSubject('Lead gerapporteerd')
            ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
            ->setTo(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
            ->setBody(
                $this->renderView(
                    'AppBundle:Emails:rapportage.html.twig',
                    array('rijschool' => $this->getUser(),
                        'lead' => $userLead)
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);

        $this->addFlash('success', 'U heeft succesvol deze lead gerapporteert. Wij zullen uw rapportage bekijken en gepaste maatregelen treffen.');
        return $this->redirectToRoute('admin_leads');
    }

    /**
     * @Route("/admin/categorieen/", name="admin_categories")
     * @Template()
     */
    public function categoryAction(Request $request)
    {

        // remove dublicate code
        $user = $this->getUser();
        $roles = $user->getRoles();
        foreach($roles as $role)
        {
            if($role === 'ROLE_ADMIN')
                return $this->redirectToRoute('adminplus_dashboard');
        }

        if($user->getStatus() === User::STATUS_DEACTIVATED)
            return $this->redirectToRoute('fos_user_security_logout');

        $wizard = $user->getWizardCompleted();
        if($wizard === User::WIZARD_NOT_COMPLETED)
            return $this->redirectToRoute('admin_wizard');

        $complete = $user->getWelcomeCompleted();
        if($complete === User::WELCOME_NOT_COMPLETED)
            return $this->redirectToRoute('admin_welcome');
        // remove dublicate code

        if($request->getMethod() == 'POST')
        {
            $licenceID = $request->get('license');
            try {
                if(is_numeric($licenceID)) {
                    $licence = $this->getDoctrine()->getRepository("AppBundle:Licence")->findOneBy(['id' => $licenceID]);
                    if(!$licence){
                        $this->addFlash('danger', 'U heeft geen rijbewijscategorie geselecteerd. Selecteer een rijbewijscategorie en probeer het opnieuw.');
                        return $this->redirectToRoute('admin_categories');
                    }

                    $userLicence = $this->getDoctrine()->getRepository('AppBundle:UserLicence')->findOneBy(['user' => $this->getUser(), 'licence' => $licence]);
                    if($userLicence) {
                        $this->addFlash('danger', 'De geselecteerde categorie is al gekoppeld aan uw account.');
                        return $this->redirectToRoute('admin_categories');
                    }

                    /**
                     * @var $user User
                     */
                    $em = $this->getDoctrine()->getManager();
                    $userLicence = new UserLicence();
                    $userLicence->setUser($this->getUser());
                    $userLicence->setLicence($licence);
                    $em->persist($userLicence);
                    $em->flush();
                    $this->addFlash('success', 'De geselecteerde categorie is successvol toegevoegd');

                }


            } catch(\Exception $e)
            {
                $this->addFlash('danger', 'Er is een onbekende fout opgetreden. Probeer het opnieuw of neem contact op met de helpdesk.');
            }

        }

        $licenses = $this->getDoctrine()->getRepository("AppBundle:Licence")->findAll();
        $userLicenses = $this->getDoctrine()->getRepository("AppBundle:UserLicence")->findBy(['user' => $this->getUser()]);

        return array(
            'userLicenses' => $userLicenses,
            'licenses' => $licenses
            );
    }

    /**
     * @Route("/admin/werkgebieden/", name="admin_cities")
     * @Template()
     */
    public function cityAction(Request $request)
    {

        // remove dublicate code
        $user = $this->getUser();
        $roles = $user->getRoles();
        foreach($roles as $role)
        {
            if($role === 'ROLE_ADMIN')
                return $this->redirectToRoute('adminplus_dashboard');
        }

        if($user->getStatus() === User::STATUS_DEACTIVATED)
            return $this->redirectToRoute('fos_user_security_logout');

        $wizard = $user->getWizardCompleted();
        if($wizard === User::WIZARD_NOT_COMPLETED)
            return $this->redirectToRoute('admin_wizard');

        $complete = $user->getWelcomeCompleted();
        if($complete === User::WELCOME_NOT_COMPLETED)
            return $this->redirectToRoute('admin_welcome');
        // remove dublicate code

        if($request->getMethod() == 'POST')
        {
            $cityID = $request->get('city');
            $licenceID = $request->get('licence');
//            $limit = $request->get('limit');

            $validator = $this->get('service.validator');

            $error = false;
            $values = [$cityID, $licenceID];
            foreach($values as $val)
            {
                if(!$validator->validateNumericValue($val))
                {
                    $error = true;
                }
                if(!$validator->validateEmptyValue($val))
                {
                    $error = true;
                }
            }

            if($error === true)
            {
                $this->addFlash('danger', 'Er is een onbekende fout opgetreden. Probeer het opnieuw.');
                return $this->redirectToRoute('admin_cities');
            }

            $city = $this->getDoctrine()->getRepository("AppBundle:City")->findOneBy(['id' => $cityID]);
            $licence = $this->getDoctrine()->getRepository("AppBundle:Licence")->findOneBy(['id' => $licenceID]);
            if(!$city){
                $this->addFlash('danger', 'Geen stad gevonden. Probeer het opnieuw.');
                return $this->redirectToRoute('admin_cities');
            }

            if(!$licence){
                $this->addFlash('danger', 'Geen rijbewijstype gevonden. Probeer het opnieuw.');
                return $this->redirectToRoute('admin_cities');
            }

            $licenceCity = $this->getDoctrine()->getRepository('AppBundle:LicenceCity')->findOneBy(['user' => $this->getUser(), 'city' => $city, 'licence' => $licence]);
            if($licenceCity){
                $this->addFlash('danger', 'De geselecteerde werkgebied/categorie is al gekoppeld aan uw account.');
                return $this->redirectToRoute('admin_cities');
            }

            /**
             * @var $user User
             */
            $em = $this->getDoctrine()->getManager();
            $licenceCity = new LicenceCity();
            $licenceCity->setUser($this->getUser());
            $licenceCity->setCity($city);
            $licenceCity->setLicence($licence);
            $licenceCity->setMaxLimit(null);

//            if($limit === 'geen')
//            {
//                $licenceCity->setMaxLimit(0);
//            } else {
//
//                if((int)$limit === 0) {
//                    $licenceCity->setMaxLimit(null);
//                } else {
//                    $licenceCity->setMaxLimit($limit);
//                }
//            }
            $em->persist($licenceCity);
            $em->flush();

            $this->addFlash('success', 'De geselecteerde werkgebied is successvol toegevoegd');
            return $this->redirectToRoute('admin_cities');

        }

        $cities = $this->getDoctrine()->getRepository("AppBundle:City")->findAll();
        $licences = $this->getDoctrine()->getRepository('AppBundle:Licence')->findAll();
        $licenceCities = $this->getDoctrine()->getRepository("AppBundle:LicenceCity")->findBy(['user' => $this->getUser()]);

        return array(
            'licenceCities' => $licenceCities,
            'cities' => $cities,
            'licences' => $licences
        );
    }

    /**
     * @Route("/admin/werkgebieden/verwijder/{licenceCityID}", name="admin_city_delete")
     */
    public function deleteCityAction($licenceCityID, Request $request)
    {

        $validator = $this->get('service.validator');
        if(!$validator->validateNumericValue($licenceCityID))
            throw new Exception("Non numeric value provided - city");

        $userCity = $this->getDoctrine()->getRepository("AppBundle:LicenceCity")->findOneBy(['id' => $licenceCityID, 'user' => $this->getUser()]);

        if(!$userCity)
        {
            $this->addFlash('danger', 'Er is een onbekende fout opgetreden. Probeer het opnieuw');
            return $this->redirectToRoute('admin_cities');
        }


        $em = $this->getDoctrine()->getManager();
        $em->remove($userCity);
        $em->flush();
        $this->addFlash('success', 'Werkgebied is succesvol verwijderd.');
        return $this->redirectToRoute('admin_cities');
    }

//    /**
//     * @Route("/admin/categorieen/verwijder/{category_id}", name="admin_category_delete")
//     */
//    public function deleteCategoryAction($category_id, Request $request)
//    {
//        $validator = $this->get('service.validator');
//        if(!$validator->validateNumericValue($category_id))
//            throw new Exception("Non numeric value provided - Category");
//
//        $userLicence = $this->getDoctrine()->getRepository("AppBundle:UserLicence")->find($category_id);
//
//        if(!$userLicence)
//            throw new \Exception('userLicence not found!');
//
//        $em = $this->getDoctrine()->getManager();
//        $em->remove($userLicence);
//        $em->flush();
//        $this->addFlash('success', 'Categorie is succesvol verwijderd.');
//        return $this->redirectToRoute('admin_categories');
//    }

    /**
     * @Route("/admin/instellingen/", name="admin_settings")
     * @Template()
     */
    public function settingsAction(Request $request)
    {

        // remove dublicate code
        $user = $this->getUser();
        $roles = $user->getRoles();
        foreach($roles as $role)
        {
            if($role === 'ROLE_ADMIN')
                return $this->redirectToRoute('adminplus_dashboard');
        }

        if($user->getStatus() === User::STATUS_DEACTIVATED)
            return $this->redirectToRoute('fos_user_security_logout');

        $wizard = $user->getWizardCompleted();
        if($wizard === User::WIZARD_NOT_COMPLETED)
            return $this->redirectToRoute('admin_wizard');

        $complete = $user->getWelcomeCompleted();
        if($complete === User::WELCOME_NOT_COMPLETED)
            return $this->redirectToRoute('admin_welcome');
        // remove dublicate code
        
        return array();
    }

//    /**
//     * @Route("/admin/instellingen/leads/", name="admin_settings_leads")
//     */
//    public function saveLeadsAction(Request $request)
//    {
//        $amount = $request->get('amount');
//        $validator = $this->get('service.validator');
//
//        if(!$validator->validateNumericValue($amount))
//            throw new Exception("non numeric value provided");
//
//        $em = $this->getDoctrine()->getManager();
//        $setting = $this->getDoctrine()->getRepository('AppBundle:Setting')->findOneBy(['name' => 'leadspm', 'user' => $this->getUser()]);
//        if(!$setting) {
//            $setting = new Setting();
//            $setting->setUser($this->getUser());
//            $setting->setName('leadspm');
//            $setting->setValue($amount);
//            $em->persist($setting);
//        } else {
//            $setting->setValue($amount);
//        }
//        $em->flush();
//        return $this->redirectToRoute('admin_settings');
//    }

    /**
     * @Route("/admin/instellingen/wachtwoord-opslaan/", name="admin_setting_password")
     */
    public function savePasswordAction(Request $request)
    {

        $error = false;
        $currentPassword = $request->get('current-password');
        $newPassword = $request->get('password');
        $newPasswordRepeat = $request->get('password-repeat');

        $emptyArray = [$currentPassword, $newPassword, $newPasswordRepeat];
        $validator = $this->get('service.validator');
        foreach($emptyArray as $item)
        {
            if(!$validator->validateEmptyValue($item))
            {
                $error = true;
            }
        }

        if(!$validator->validatePasswordComplexity($newPassword)){
            $this->addFlash('danger', 'Uw ingevulde wachtwoord voldoet niet aan de veiligheidseisen van minimaal 8 karakters, 1 hoofletter en 1 getal. ');
            return $this->redirectToRoute('admin_settings');
        }

        if($error == true)
        {
            $this->addFlash('danger', 'U heeft een of meerdere velden niet ingevuld. Probeer het opnieuw.');
            return $this->redirectToRoute('admin_settings');
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($this->getUser());
        if(!$encoder->isPasswordValid($this->getUser()->getPassword(), $currentPassword, $this->getUser()->getSalt()))
        {
            $this->addFlash('danger', "Ingevulde huidige wachtwoord komt niet overeen. Probeer het opnieuw.");
            return $this->redirectToRoute('admin_settings');
        }

        if($newPassword !== $newPasswordRepeat)
        {
            $this->addFlash('danger', 'De nieuwe ingevulde wachtwoorden komen niet overeen. Probeer het opnieuw.');
            return $this->redirectToRoute('admin_settings');
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($this->getUser());
        $newPasswordHashed = $encoder->encodePassword($newPasswordRepeat, $this->getUser()->getSalt());

        $this->getUser()->setPassword($newPasswordHashed);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $this->addFlash('success', 'Uw wachtwoord is succesvol geupdate.');
        return $this->redirectToRoute('admin_settings');

    }

    /**
     * @Route("/admin/instellingen/details-opslaan/", name="admin_setting_details")
     */
    public function saveDetailsAction(Request $request)
    {
        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $phoneNumber = $request->get('phonenumber');
        $companyName = $request->get('companyname');
        $address = $request->get('address');
        $houseNumber = $request->get('housenumber');
        $zipcode = str_replace(' ', '', $request->get('zipcode'));
        $city = $request->get('city');

        $error = false;
        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('service.validator');
        $values = [$firstname, $lastname, $phoneNumber, $companyName, $address, $houseNumber, $zipcode, $city];
        $typeError = '';


        foreach($values as $value)
        {
            if(!$validator->validateEmptyValue($value))
            {
                $error = true;
                $typeError .= $value;
            }

        }

        if(!$validator->validateZipCode($zipcode))
        {
            $error = true;
            $typeError .= 'zipcode222';
        }


        if(!$validator->validatePhoneNumber($phoneNumber))
        {
            $error = true;
            $typeError .= 'phonenumber';
        }


        if($error === true)
        {
            $user = $this->getUser();
            $user->setStatus(User::STATUS_INACTIVE);
            $em->flush();
            $this->addFlash('danger', "U heeft een of meerdere fouten in het formulier. Verbeter de fouten en probeer het opnieuw.");
        } else {
            /**
             * @var $user User
             */
            $user = $this->getUser();
            $user->setFirstName($firstname);
            $user->setLastName($lastname);
            $user->setPhoneNumber($phoneNumber);
            $user->setCompanyName($companyName);
            $user->setAddress($address);
            $user->setHouseNumber($houseNumber);
            $user->setZipcode($zipcode);
            $user->setCity($city);
            $user->setStatus(User::STATUS_ACTIVATING);
            $em->flush();
            $this->addFlash('success', 'Uw gegevens zijn succesvol geupdate. Uw account wordt momenteel gevalideerd.');
        }

        return $this->redirectToRoute('admin_settings');

    }

    /**
     * @Route("/admin/veelgestelde-vragen",name="admin_help")
     * @Template()
     */
    public function helpAction(Request $request)
    {
        // remove dublicate code
        $user = $this->getUser();
        $roles = $user->getRoles();
        foreach($roles as $role)
        {
            if($role === 'ROLE_ADMIN')
                return $this->redirectToRoute('adminplus_dashboard');
        }

        if($user->getStatus() === User::STATUS_DEACTIVATED)
            return $this->redirectToRoute('fos_user_security_logout');

        $wizard = $user->getWizardCompleted();
        if($wizard === User::WIZARD_NOT_COMPLETED)
            return $this->redirectToRoute('admin_wizard');

        $complete = $user->getWelcomeCompleted();
        if($complete === User::WELCOME_NOT_COMPLETED)
            return $this->redirectToRoute('admin_welcome');
        // remove dublicate code

        return [];
    }

    /**
     * @Route("/admin/welcome/", name="admin_welcome")
     * @Template()
     */
    public function welcomeAction(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            $welcome = (int)$request->get('welcome');
            if($welcome === User::WELCOME_COMPLETED)
            {
                $user = $this->getUser();
                $user->setWelcomeCompleted(User::WELCOME_COMPLETED);
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return $this->redirectToRoute('admin_dashboard');
            } else {
                $this->addFlash('danger', 'Bevestig dat u introductie tekst heeft gelezen.');
                return [];
            }
        }

        return [];
    }

    /**
     * @Route("/admin/instellingen/deactivate/", name="admin_setting_deactivate")
     */
    public function deactivateAction(Request $request)
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        $user->setStatus(User::STATUS_DEACTIVATED);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $this->addFlash('success', 'Uw account is succesvol gedeactiveerd. U ontvangt per direct geen offerteaanvragen meer.');

        return $this->redirectToRoute('admin_settings');

    }

    /**
     * @Route("/admin/instellingen/activate/", name="admin_setting_activate")
     */
    public function activateAction(Request $request)
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        $user->setStatus(User::STATUS_ACTIVATED);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $this->addFlash('success', 'Uw account is succesvol geactiveerd. U ontvangt per direct weer offerteaanvragen.');

        return $this->redirectToRoute('admin_settings');

    }

}
