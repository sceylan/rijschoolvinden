<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Setting;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCity;
use AppBundle\Entity\UserLicence;
use AppBundle\Entity\Lead;
use AppBundle\Entity\UserLead;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminPlusController extends Controller
{

    /**
     * @Route("/adminplus/dashboard/", name="adminplus_dashboard")
     * @Template()
     */
    public function dashboardAction(Request $request)
    {
        $query = $this->getDoctrine()->getEntityManager()->createQuery('SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role');
        $query->setParameter('role', '%"ROLE_RIJSCHOOL"%');

        $rijscholen = $query->getResult();
        return array('rijscholen' => $rijscholen);
    }

    /**
     * @Route("/adminplus/rijscholen/", name="adminplus_rijscholen")
     * @Template()
     */
    public function rijscholenAction(Request $request)
    {
        $query = $this->getDoctrine()->getEntityManager()->createQuery('SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role ORDER BY u.registrationDate DESC');
        $query->setParameter('role', '%"ROLE_RIJSCHOOL"%');

        $rijscholen = $query->getResult();

        $activeUsers = $this->getDoctrine()->getEntityManager()->createQuery('SELECT count(u) FROM AppBundle:User u WHERE u.status = 2 ')->getSingleScalarResult();
        $nonActiveUsers = $this->getDoctrine()->getEntityManager()->createQuery('SELECT count(u) FROM AppBundle:User u WHERE u.status != 2 ')->getSingleScalarResult();
        $nonWizardUsers = $this->getDoctrine()->getEntityManager()->createQuery('SELECT count(u) FROM AppBundle:User u WHERE u.wizardCompleted = 0 ')->getSingleScalarResult();
        $nonWelcomeUsers = $this->getDoctrine()->getEntityManager()->createQuery('SELECT count(u) FROM AppBundle:User u WHERE u.welcomeCompleted = 0 ')->getSingleScalarResult();

        return array(
            'rijscholen' => $rijscholen,
            'activeUsers' => $activeUsers,
            'nonActiveUsers' => $nonActiveUsers,
            'nonWizardUsers' => $nonWizardUsers,
            'nonWelcomeUsers' => $nonWelcomeUsers
            );
    }

    /**
     * @Route("/adminplus/rijschool/{id}", name="adminplus_rijschool")
     * @Template()
     */
    public function rijschoolAction(Request $request, $id)
    {
        $rijschool = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        $licenceCities = $this->getDoctrine()->getRepository("AppBundle:LicenceCity")->findBy(['user' => $rijschool]);

        return [
            'rijschool' => $rijschool,
            'licenceCities' => $licenceCities
        ];
    }

    /**
     * @Route("/adminplus/werkgebieden/", name="adminplus_werkgebieden")
     * @Template()
     */
    public function werkgebiedenAction(Request $request)
    {
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

        $results = $qb->select('count(lc) as total, IDENTITY(lc.city) as cityID')
            ->from('AppBundle:LicenceCity', 'lc')
            ->innerJoin('lc.city', 'c')
            ->groupBy('lc.city')
            ->orderBy('total', 'DESC')
            ->getQuery()
            ->getResult();

        $totalCounts = [];
        $cities = [];
        $rijscholen = [];

        foreach($results as $res)
        {
            $cityID = $res['cityID'];
            $count = $res['total'];
            $cityObject = $this->getDoctrine()->getRepository('AppBundle:City')->find($cityID);
            $licenceCities = $this->getDoctrine()->getRepository('AppBundle:LicenceCity')->findBy(['city' => $cityObject]);

            foreach($licenceCities as $licenceCity)
            {
                $rijscholen[$cityObject->getId()] = [];
                $rijscholen[$cityObject->getId()]['users'] = $licenceCity->getUser();
            }
            $cities[]  = $cityObject;
            $totalCounts[$cityObject->getId()] = $count;
        }

        return array('werkgebieden' => $cities, 'total' => $totalCounts, 'rijscholen' => $rijscholen);
    }

    /**
     * @Route("/adminplus/werkgebied/{cityID}/", name="adminplus_werkgebied")
     * @Template()
     */
    public function werkgebiedAction(Request $request, $cityID)
    {
        $rijscholen = [];
        $licenceCities = $this->getDoctrine()->getRepository('AppBundle:LicenceCity')->findBy(['city' => $cityID]);
        $city = $this->getDoctrine()->getRepository('AppBundle:City')->find($cityID);

        foreach($licenceCities as $licenceCity)
        {
            $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($licenceCity->getUser());
            $rijscholen[] = $user;
        }

        return array('city' => $city,'rijscholen' => $rijscholen);
    }

    /**
     * @Route("/adminplus/lead/{id}", name="adminplus_lead")
     * @Template()
     */
    public function leadAction(Request $request, $id)
    {
        $lead = $this->getDoctrine()->getRepository('AppBundle:Lead')->find($id);
        $userLeads = $this->getDoctrine()->getRepository('AppBundle:UserLead')->findBy(['lead' => $lead]);
        return ['lead' => $lead, 'userLeads' => $userLeads];
    }

    /**
     * @Route("/adminplus/rijschool/save/{id}", name="adminplus_save_user")
     * @Template()
     */
    public function saveRijschoolFunction(Request $request, $id)
    {

        if($request->getMethod() == 'POST')
        {

            $validator = $this->get('service.validator');
            if(!$validator->validateNumericValue($id))
            {
                $this->addFlash('danger', 'Onbekend rijschool opgegeven');
                return $this->redirectToRoute('adminplus_rijscholen');
            }

            $rijschool = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

            if(!$rijschool)
            {
                $this->addFlash('danger', 'Geen rijschool opgegeven');
                return $this->redirectToRoute('adminplus_rijscholen');
            }

            $firstname = $request->get('firstname');
            $lastname = $request->get('lastname');
            $phoneNumber = $request->get('phonenumber');
            $companyName = $request->get('companyname');
            $address = $request->get('address');
            $houseNumber = $request->get('housenumber');
            $zipcode = str_replace(' ', '', $request->get('zipcode'));
            $city = $request->get('city');

            $rijschool->setFirstName($firstname);
            $rijschool->setLastName($lastname);
            $rijschool->setPhoneNumber($phoneNumber);
            $rijschool->setCompanyName($companyName);
            $rijschool->setAddress($address);
            $rijschool->setHouseNumber($houseNumber);
            $rijschool->setZipcode($zipcode);
            $rijschool->setCity($city);

            $em = $this->getDoctrine()->getEntityManager();
            $em->flush();

            $this->addFlash('success', 'Rijschool gegevens succesvol opgeslagen.');
            return $this->redirectToRoute('adminplus_rijschool', ['id' => $id]);

        }

        $this->addFlash('danger', 'Er is een onbekende fout opgetreden. Probeer het opnieuw.');
        return $this->redirectToRoute('adminplus_rijscholen');

    }

    /**
     * @Route("/adminplus/werkgebieden/verwijder/{city_id}", name="adminplus_city_delete")
     */
    public function deleteCityAction($city_id, Request $request)
    {
        $validator = $this->get('service.validator');
        if(!$validator->validateNumericValue($city_id))
            throw new Exception("Non numeric value provided - city");

        $userCity = $this->getDoctrine()->getRepository("AppBundle:LicenceCity")->find($city_id);
        $rijschool = $userCity->getUser();

        if(!$userCity)
            throw new \Exception('LicenceCity not found!');

        $em = $this->getDoctrine()->getManager();
        $em->remove($userCity);
        $em->flush();
        $this->addFlash('success', 'Werkgebied is succesvol verwijderd.');
        return $this->redirectToRoute('adminplus_rijschool', ['id' => $rijschool->getId()]);
    }

    /**
     * @Route("/adminplus/leads/", name="adminplus_leads")
     * @Template()
     */
    public function leadsAction(Request $request)
    {
        $leads = $this->getDoctrine()->getRepository('AppBundle:Lead')->findBy([], ['inserted' => 'DESC']);
        return array('leads' => $leads);
    }

    /**
     * @Route("/adminplus/verzonden-leads/", name="adminplus_leads_send")
     * @Template()
     */
    public function sendLeadsAction(Request $request)
    {
        $userLeads = $this->getDoctrine()->getRepository('AppBundle:UserLead')->findBy([], ['dateTimeEmailed' => 'DESC']);
        return array('leads' => $userLeads);
    }

    /**
     * @Route("/adminplus/lead/save/{id}", name="adminplus_save_lead")
     * @Template()
     */
    public function saveLeadAction(Request $request, $id)
    {
        if($request->getMethod() == 'POST')
        {

            $validator = $this->get('service.validator');
            if(!$validator->validateNumericValue($id))
            {
                $this->addFlash('danger', 'Onbekend rijschool opgegeven');
                return $this->redirectToRoute('adminplus_leads');
            }

            $lead = $this->getDoctrine()->getRepository('AppBundle:Lead')->find($id);

            if(!$lead)
            {
                $this->addFlash('danger', 'Geen lead opgegeven');
                return $this->redirectToRoute('adminplus_leads');
            }

            $firstname = $request->get('firstname');
            $lastname = $request->get('lastname');
            $phoneNumber = $request->get('phonenumber');
            $emailAddress = $request->get('emailaddress');

            $lead->setFirstName($firstname);
            $lead->setLastName($lastname);
            $lead->setPhoneNumber($phoneNumber);
            $lead->setEmailAddress($emailAddress);

            $em = $this->getDoctrine()->getEntityManager();
            $em->flush();

            $this->addFlash('success', 'Lead gegevens succesvol opgeslagen.');
            return $this->redirectToRoute('adminplus_lead', ['id' => $id]);

        }

        $this->addFlash('danger', 'Er is een onbekende fout opgetreden. Probeer het opnieuw.');
        return $this->redirectToRoute('adminplus_leads');
    }

    /**
     * @Route("/adminplus/gerapporteerde-leads/", name="adminplus_rapportages")
     * @Template()
     */
    public function rapportagesAction(Request $request)
    {
        $userLeads = $this->getDoctrine()->getRepository('AppBundle:UserLead')->findBy(['reported' => UserLead::STATUS_REPORTED], ['dateTimeEmailed' => 'DESC']);
        return array('leads' => $userLeads);
    }

    /**
     * @Route("/adminplus/activate/{id}", name="adminplus_activate_account")
     * @Template()
     */
    public function activateAccountAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if(!$user)
            return $this->redirectToRoute('adminplus_rijscholen');

        $user->setEnabled(true);
        $user->setStatus(User::STATUS_ACTIVATED);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $message = \Swift_Message::newInstance()
            ->setSubject('Account gevalideerd!')
            ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
            ->setTo([$user->getEmail() => $user->getFirstName() . ' ' . $user->getLastName()])
            ->setBody(
                $this->renderView(
                    'AppBundle:Emails:validated.html.twig',
                    array('companyName' => $user->getCompanyName())
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);

        // send email


        return $this->redirectToRoute('adminplus_rijschool', ['id' => $id]);
    }


    /**
     * @Route("/adminplus/deactivate/{id}", name="adminplus_deactivate_account")
     * @Template()
     */
    public function deactivateAccountAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if(!$user)
            return $this->redirectToRoute('adminplus_rijscholen');

        $user->setEnabled(false);
        $user->setStatus(User::STATUS_DEACTIVATED);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('adminplus_rijschool', ['id' => $id]);
    }

    /**
     * @Route("/adminplus/validate-report/{userlead_id}/{status}", name="adminplus_validate_report")
     * @Template()
     */
    public function validateReportAction(Request $request, $userlead_id, $status)
    {
        $userLead = $this->getDoctrine()->getRepository('AppBundle:UserLead')->find($userlead_id);
        $lead = $userLead->getLead();

        if(!$userLead)
            throw new \Exception('No userlead found!');

        switch($status)
        {
            case 0:
                // do not credit lead
                $userLead->setReported(UserLead::STATUS_VALID);
                break;
            case 1:
                // credit lead
                $userLead->setReported(UserLead::STATUS_INVALID);
                break;
            default:
                break;

        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('adminplus_lead', ['id' => $lead->getId()]);

    }

    /**
     * @Route("/adminplus/disable-lead/{leadID}", name="adminplus_disable_lead")
     * @Template()
     */
    public function disableLeadAction(Request $request, $leadID)
    {
        $lead = $this->getDoctrine()->getRepository('AppBundle:Lead')->find($leadID);

        if(!$lead)
            throw new \Exception('No userlead found!');

        $lead->setStatus(Lead::STATUS_INVALID);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('adminplus_lead', ['id' => $lead->getId()]);

    }



}
