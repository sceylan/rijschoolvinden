<?php

namespace AppBundle\Controller;

use AppBundle\Entity\City;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LeerlingController extends Controller
{

    /**
     * @Route("/hoe-werkt-het/", name="leerling_werking")
     * @Template()
     */
    public function werkingAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/kosten/", name="leerling_kosten")
     * @Template()
     */
    public function kostenAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/privacy/", name="leerling_privacy")
     * @Template()
     */
    public function privacyAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/veelgestelde-vragen/", name="leerling_vragen")
     * @Template()
     */
    public function vragenAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/klachten/", name="leerling_klachten")
     * @Template()
     */
    public function klachtenAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/rijschool/steden/", name="leerling_steden")
     * @Template()
     */
    public function stedenAction(Request $request)
    {
        $cities = $this->getDoctrine()->getRepository('AppBundle:City')->findAll();
        return array('cities' => $cities);
    }

    /**
     * @Route("/rijschool/{city}/", name="leerling_stad")
     * @Template()
     */
    public function stadAction(Request $request, $city)
    {

        if (strpos($city, '-') !== false) {
            $city1 = $this->getDoctrine()->getRepository('AppBundle:City')->findOneBy(['name' => $city]);
            if(!$city1) {
                $city1Cleaned = str_replace('-', ' ', $city);
                $city2 = $this->getDoctrine()->getRepository('AppBundle:City')->findOneBy(['name' => $city1Cleaned]);
                if(!$city2) {
                    return $this->redirectToRoute('website_home');
                } else {
                    return array('city' => $city2);
                }
            } else {
                return array('city' => $city1);
            }
        } else {
            // city might contain spaces, replace spaces with -
            $cityCleaned = str_replace('-', ' ', $city);
        }

        $city = $this->getDoctrine()->getRepository('AppBundle:City')->findOneBy(['name' => $cityCleaned]);

        if(!$city)
            return $this->redirectToRoute('website_home');

        return array('city' => $city);
    }

}
