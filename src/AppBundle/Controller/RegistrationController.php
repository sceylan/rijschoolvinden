<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;

class RegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        if($request->isMethod('POST')) {
            // validate the action, else return registration page.
            $action = $request->get('action');
            if($action == 'registration') {

                // start validation all attributes
                $error = false;
                $values = array(
                    'email' => 'Email adres',
                    'phonenumber' => 'Telefoonnummer',
                    'companyname' => 'Bedrijfsnaam');

                $validatedValues = array(
                    'email' => '',
                    'phonenumber' => '',
                    'companyname' => '');

                $validator = $this->get('service.validator');

                foreach($values as $key => $label) {
                    $inputField = $request->get($key);
                    $validatedValues[$key] = $inputField;

                    if(!$validator->validateEmptyValue($inputField))
                        $error = true;
                }

                if(!$validator->validateEmail($validatedValues['email']))
                    $error = true;

                if(!$validator->validateNumericValue($validatedValues['phonenumber']))
                    $error = true;

                if($error == true)
                {
                    $errorMessage = 'U heeft mogelijk één of meerdere velden incorrect ingevuld. Controleer uw invoer en probeer het opnieuw.';
                    $this->addFlash('danger', $errorMessage);
                }

                $userManager = $this->container->get('fos_user.user_manager');
                $account = $userManager->findUserByUsernameOrEmail($validatedValues['email']);
                if ($account) {

//                    if(!$account->isEnabled())
//                    {
//                        $account->setEnabled(1);
//                        $account->setStatus(User::STATUS_ACTIVATING);
//                        $em = $this->getDoctrine()->getManager();
//                        $em->flush();
//
//                        $message = \Swift_Message::newInstance()
//                            ->setSubject('Heractivatie rijschool')
//                            ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
//                            ->setTo(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
//                            ->setBody(
//                                $this->renderView(
//                                    'AppBundle:Emails:validatie.html.twig',
//                                    array('companyName' => $account->getCompanyName(),
//                                        'city' => $account->getCity(),
//                                        'firstName' => $account->getFirstName(),
//                                        'lastName' => $account->getLastName(),
//                                        'amountLeads' => 0)
//                                ),
//                                'text/html'
//                            )
//                        ;
//                        $this->get('mailer')->send($message);
//
//                        $this->addFlash('success', "Uw account is opnieuw geactiveerd. U kunt inloggen met uw oude wachtwoord. Wachtwoord vergeten? Herstel dan uw wachtwoord.");
//                        return $this->redirectToRoute('fos_user_security_login');
//                    }

                    // Account bestaat al.
                    $errorMessage = 'Het opgegeven e-mail adres komt al voor in ons systeem. Bent u uw wachtwoord vergeten? Probeer uw wachtwoord te herstellen.';
                    $this->addFlash('danger', $errorMessage);
                    $error = true;
                }

                if($error == false) {
                    $email = $validatedValues['email'];
                    $companyName = $validatedValues['companyname'];
                    $phoneNumber = $validatedValues['phonenumber'];

                    /**
                     * @var $user User
                     */
                    $user = $userManager->createUser();
                    $factory = $this->get('security.encoder_factory');
                    $encoder = $factory->getEncoder($user);
                    $password = $this->randomPassword();
                    $password_hashed = $encoder->encodePassword($password, $user->getSalt());
                    $user->setUserName($email);
                    $user->setEmail($email);
                    $user->setPassword($password_hashed);
                    $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                    $user->setConfirmationToken($tokenGenerator->generateToken());
                    $user->addRole('ROLE_RIJSCHOOL');

                    $user->setPhoneNumber($phoneNumber);
                    $user->setCompanyName($companyName);
                    $user->setStatus(User::STATUS_INACTIVE);
                    $user->setEnabled(1);
                    $user->setWizardCompleted(User::WIZARD_NOT_COMPLETED);
                    $user->setWelcomeCompleted(User::WELCOME_NOT_COMPLETED);
                    $user->setRegistrationDate(time());
                    $user->setLastUpdatedDate(time());

                    $userManager->updateUser($user);

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Uw registratie bij Rijschoolvinden')
                        ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                        ->setTo([$email => $companyName])
                        ->setBody(
                            $this->renderView(
                                'AppBundle:Emails:registration.html.twig',
                                array('companyName' => $companyName,
                                    'email' => $email,
                                    'password' => $password,
                                    'token' => $user->getConfirmationToken())
                            ),
                            'text/html'
                        )
                    ;
                    $this->get('mailer')->send($message);
                    $this->addFlash('success', 'U bent succesvol geregistreerd! U ontvangt per e-mail uw inloggegevens.');
                    return $this->redirectToRoute('fos_user_security_login');
                }
            }
        }
        return $this->render('FOSUserBundle:Registration:register.html.twig');
    }

    private function randomPassword() {
        $alphabet = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}