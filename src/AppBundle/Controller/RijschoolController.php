<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RijschoolController extends Controller
{

    /**
     * @Route("/rijscholen/informatie-en-kosten/", name="rijschool_informatie")
     * @Template()
     */
    public function informatieAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/rijscholen/kosten/", name="rijschool_kosten")
     * @Template()
     */
    public function kostenAction(Request $request)
    {
        return array();
    }

}
