<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Lead;

class WebsiteController extends Controller
{

    /**
     * @Route("/", name="website_home")
     * @Template()
     */
    public function homeAction(Request $request)
    {

        if($request->getMethod() == 'POST')
        {
            // Gather data from request
            $licenceID = $request->get('license');
            $cityID = $request->get('city');

            $name = $request->get('voornaam');
            $lastName = $request->get('achternaam');
            $email = $request->get('email');
            $phone = $request->get('telefoon');

            $experience = $request->get('rijervaring');
            $startAvailability = $request->get('beginmogelijkheid');
            $contactPreference = $request->get('contactvoorkeur');

            // Start validating data
            $error = false;
            $validator = $this->get('service.validator');
            $values = [$licenceID, $cityID, $name, $lastName, $email, $phone, $experience, $startAvailability, $contactPreference];


            // Validate all values if they are empty or not.
            foreach($values as $value)
            {
                if(!$validator->validateEmptyValue($value))
                {
                    $error = true;
                }
            }

            // Validate email && domainname
            if(!$validator->validateEmail($email) || !$validator->validateDomainName($email))
            {
                $error = true;
//                $this->addFlash('danger', 'Het ingevoerde e-mailadres is onjuist. Graag corrigeren en opnieuw proberen.');
            }

            // Validate phone number dutch regex
            if(!$validator->validatePhoneNumber($phone))
            {
                $error = true;
//                $this->addFlash('danger', 'Het telefoonnummer is ongeldig. Graag corrigeren en opnieuw proberen.');
            }

            if($error === true)
            {
                $this->addFlash('danger', 'Een of meer velden bevatten een fout. Graag corrigeren en opnieuw proberen.');
            }

            if($error === false)
            {
                $licence = $this->getDoctrine()->getRepository("AppBundle:Licence")->find($licenceID);
                $city = $this->getDoctrine()->getRepository("AppBundle:City")->find($cityID);

                if(!$licence || !$city)
                    throw new \Exception("No licence or city found");


                //Start building lead object;
                $lead = new Lead();
                $lead->setFirstName($name);
                $lead->setLastName($lastName);
                $lead->setEmailAddress($email);
                $lead->setPhoneNumber($phone);
                $lead->setLicence($licence);
                $lead->setCity($city);
                $lead->setInserted(time());
                $lead->setStatus(Lead::STATUS_NEW);

                switch($experience)
                {
                    case 0:
                        $lead->setExperience(Lead::EXPERIENCE_NO);
                        break;
                    case 1:
                        $lead->setExperience(Lead::EXPERIENCE_YES);
                        break;
                    default:
                        throw new \Exception('Experience exception occured!');
                }

                switch($startAvailability)
                {
                    case 0:
                        $lead->setAvailability(Lead::AVAILABILITY_ASAP);
                        break;
                    case 1:
                        $lead->setAvailability(Lead::AVAILABILITY_ONE_WEEK);
                        break;
                    case 2:
                        $lead->setAvailability(Lead::AVAILABILITY_TWO_WEEKS);
                        break;
                    case 3:
                        $lead->setAvailability(Lead::AVAILABILITY_FOUR_WEEKS);
                        break;
                    case 4:
                        $lead->setAvailability(Lead::AVAILABILITY_ONE_MONTH_ABOVE);
                        break;
                    default:
                        throw new \Exception('Availability exception occured!');
                }

                switch($contactPreference)
                {
                    case 0:
                        $lead->setContactPreference(Lead::CONTACT_PREFERENCE_PHONE);
                        break;
                    case 1:
                        $lead->setContactPreference(Lead::CONTACT_PREFERENCE_EMAIL);
                        break;
                    case 2:
                        $lead->setContactPreference(Lead::CONTACT_PREFERENCE_WHATSAPP);
                        break;
                    case 3:
                        $lead->setContactPreference(Lead::CONTACT_PREFERENCE_POST);
                        break;
                    case 4:
                        $lead->setContactPreference(Lead::CONTACT_NO_PREFERENCE);
                        break;
                    default:
                        throw new \Exception('Contact exception occured!');
                }


                $em = $this->getDoctrine()->getManager();
                $em->persist($lead);
                $em->flush();
                return $this->redirectToRoute('website_success');
            }

        }

        $cities = $this->getDoctrine()->getRepository("AppBundle:City")->findAll();
        $licences = $this->getDoctrine()->getRepository("AppBundle:Licence")->findAll();
        return array(
            'cities' => $cities,
            'licences' => $licences);
    }

    /**
     * @Route("/over-rijschoolvinden/", name="website_over")
     * @Template()
     */
    public function overAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/informatie-voor-rijscholen/", name="website_informatie")
     * @Template()
     */
    public function informatieAction(Request $request)
    {
        if($request->isMethod('POST')) {
            $phonenumber = $request->get('phonenumber');
            $companyname = $request->get('companyname');
            $error = false;

            $validator = $this->get('service.validator');
            if($validator->validateEmptyValue($phonenumber) === false){
                $error = true;
            }
            if($validator->validatePhoneNumber($phonenumber) === false){
                $error = true;
            }
            if($validator->validateEmptyValue($companyname) === false){
                $error = true;
            }

            if($error === false)
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Bel mij terug verzoek website')
                    ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setTo(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setContentType("text/html")
                    ->setBody(
                        $this->renderView(
                            'AppBundle:Emails:belmij.html.twig',
                            array('phonenumber' => $phonenumber,
                                'companyname' => $companyname
                            )
                        ));
                $this->get('mailer')->send($message);
                $this->addFlash('success', 'Uw bel mij verzoek is succesvol verstuurd. Wij proberen zo spoedig mogelijk contact met u op te nemen.');
                return $this->redirectToRoute('website_informatie');
            } else {
                $this->addFlash('danger', 'Er heeft een onbekende fout plaatsgevonden. Uw bericht is niet verzonden.');
                return $this->redirectToRoute('website_informatie');
            }
        }

        return array();
    }

    /**
     * @Route("/contact/", name="website_contact")
     * @Template()
     */
    public function contactAction(Request $request)
    {
        if($request->isMethod('POST')) {
            $name = $request->get('name');
            $email = $request->get('email');
            $message = $request->get('message');

            $error = false;

            $validator = $this->get('service.validator');
            if($validator->validateEmptyValue($name) === false){
                $error = true;
                $typeError = 'name';
            }
            if($validator->validateEmptyValue($email) === false){
                $error = true;
                $typeError = 'email';
            }
            if($validator->validateEmptyValue($message) === false) {
                $error = true;
                $typeError = 'message';
            }
            if($validator->validateEmail($email) === false) {
                $error = true;
                $typeError = 'email';
            }

            if($error === false)
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact verzoek website')
                    ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setTo(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setReplyTo([$email => $name])
                    ->setContentType("text/html")
                    ->setBody(
                        $this->renderView(
                            'AppBundle:Emails:contact.html.twig',
                            array('naam' => $name,
                                'email' => $email,
                                'message' => $message
                            )
                        ));
                $this->get('mailer')->send($message);
                $this->addFlash('success', 'Uw contact verzoek is succesvol verstuurd. Wij proberen zo spoedig mogelijk contact met u op te nemen.');
                return $this->redirectToRoute('website_contact');
            } else {
                $this->addFlash('danger', 'Er heeft een onbekende fout plaatsgevonden. Uw bericht is niet verzonden.');
                return $this->redirectToRoute('website_contact');
            }
        }
        return array();

    }

    /**
     * @Route("/algemene-voorwaarden/", name="website_voorwaarden")
     * @Template()
     */
    public function voorwaardenAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/privacy-policy/", name="website_privacy")
     * @Template()
     */
    public function privacyAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/save-lead/", name="website_lead")
     * @Template()
     */
    public function leadAction(Request $request)
    {


    }

    /**
     * @Route("/aanvraag-voltooid/", name="website_success")
     * @Template()
     */
    public function successAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/sitemap.{_format}/", defaults={"_format": "xml"}, name="website_sitemap")
     * @Template()
     */
    public function sitemapAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/citymap.{_format}/", defaults={"_format": "xml"}, name="website_citymap")
     * @Template()
     */
    public function citymapAction(Request $request)
    {
        return array();
    }

}
