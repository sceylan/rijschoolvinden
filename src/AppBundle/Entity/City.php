<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity
 * @ORM\Table(name="cities")
 */
class City
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $provinceName;

    /**
     * @OneToMany(targetEntity="LicenceCity", mappedBy="city")
     */
    protected $cities;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Lead", mappedBy="city")
     */
    protected $leads;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\UserLead", mappedBy="city")
     */
    protected $userLead;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cities = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add city
     *
     * @param \AppBundle\Entity\User $city
     *
     * @return City
     */
    public function addCity(\AppBundle\Entity\User $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param \AppBundle\Entity\User $city
     */
    public function removeCity(\AppBundle\Entity\User $city)
    {
        $this->cities->removeElement($city);
    }

    /**
     * Get cities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Set provinceName
     *
     * @param string $provinceName
     *
     * @return City
     */
    public function setProvinceName($provinceName)
    {
        $this->provinceName = $provinceName;

        return $this;
    }

    /**
     * Get provinceName
     *
     * @return string
     */
    public function getProvinceName()
    {
        return $this->provinceName;
    }

    /**
     * Add lead
     *
     * @param \AppBundle\Entity\Lead $lead
     *
     * @return City
     */
    public function addLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \AppBundle\Entity\Lead $lead
     */
    public function removeLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads->removeElement($lead);
    }

    /**
     * Get leads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeads()
    {
        return $this->leads;
    }

    /**
     * Add userLead
     *
     * @param \AppBundle\Entity\UserLead $userLead
     *
     * @return City
     */
    public function addUserLead(\AppBundle\Entity\UserLead $userLead)
    {
        $this->userLead[] = $userLead;

        return $this;
    }

    /**
     * Remove userLead
     *
     * @param \AppBundle\Entity\UserLead $userLead
     */
    public function removeUserLead(\AppBundle\Entity\UserLead $userLead)
    {
        $this->userLead->removeElement($userLead);
    }

    /**
     * Get userLead
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserLead()
    {
        return $this->userLead;
    }
}
