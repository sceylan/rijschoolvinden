<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity
 * @ORM\Table(name="leads")
 */
class Lead
{

    const EXPERIENCE_NO = 0;
    const EXPERIENCE_YES = 1;

    const AVAILABILITY_ASAP = 0;
    const AVAILABILITY_ONE_WEEK = 1;
    const AVAILABILITY_TWO_WEEKS = 2;
    const AVAILABILITY_FOUR_WEEKS = 3;
    const AVAILABILITY_ONE_MONTH_ABOVE = 4;

    const CONTACT_PREFERENCE_PHONE = 0;
    const CONTACT_PREFERENCE_EMAIL = 1;
    const CONTACT_PREFERENCE_WHATSAPP = 2;
    const CONTACT_PREFERENCE_POST = 3;
    const CONTACT_NO_PREFERENCE = 4;

    const STATUS_NEW = 0;
    const STATUS_EMAILED = 1;
    const STATUS_REPORTED = 2;
    const STATUS_VALID = 3;
    const STATUS_INVALID = 4;
    const STATUS_EXPIRED = 5;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Licence", inversedBy="leads")
     */
    protected $licence;

    /**
     * @ManyToOne(targetEntity="City", inversedBy="leads")
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $firstName;
    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $lastName;
    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $emailAddress;
    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="integer")
     */
    protected $experience;

    /**
     * @ORM\Column(type="integer")
     */
    protected $availability;

    /**
     * @ORM\Column(type="integer")
     */
    protected $contactPreference;

    /**
     * @ORM\Column(type="integer")
     */
    protected $inserted;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\UserLead", mappedBy="lead")
     */
    protected $leads;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Lead
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Lead
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     *
     * @return Lead
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Lead
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set experience
     *
     * @param integer $experience
     *
     * @return Lead
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return integer
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set availability
     *
     * @param integer $availability
     *
     * @return Lead
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return integer
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Set contactPreference
     *
     * @param integer $contactPreference
     *
     * @return Lead
     */
    public function setContactPreference($contactPreference)
    {
        $this->contactPreference = $contactPreference;

        return $this;
    }

    /**
     * Get contactPreference
     *
     * @return integer
     */
    public function getContactPreference()
    {
        return $this->contactPreference;
    }

    /**
     * Set licence
     *
     * @param \AppBundle\Entity\Licence $licence
     *
     * @return Lead
     */
    public function setLicence(\AppBundle\Entity\Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return \AppBundle\Entity\Licence
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Lead
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->leads = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lead
     *
     * @param \AppBundle\Entity\Lead $lead
     *
     * @return Lead
     */
    public function addLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \AppBundle\Entity\Lead $lead
     */
    public function removeLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads->removeElement($lead);
    }

    /**
     * Get leads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeads()
    {
        return $this->leads;
    }

    /**
     * Set inserted
     *
     * @param integer $inserted
     *
     * @return Lead
     */
    public function setInserted($inserted)
    {
        $this->inserted = $inserted;

        return $this;
    }

    /**
     * Get inserted
     *
     * @return integer
     */
    public function getInserted()
    {
        return $this->inserted;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Lead
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
