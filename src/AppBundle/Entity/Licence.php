<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity
 * @ORM\Table(name="licenses")
 */
class Licence
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $name;

    /**
     * @OneToMany(targetEntity="LicenceCity", mappedBy="licence")
     */
    protected $licenses;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Lead", mappedBy="licence")
     */
    protected $leads;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->licenses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Licence
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add license
     *
     * @param \AppBundle\Entity\Licence $license
     *
     * @return Licence
     */
    public function addLicense(\AppBundle\Entity\Licence $license)
    {
        $this->licenses[] = $license;

        return $this;
    }

    /**
     * Remove license
     *
     * @param \AppBundle\Entity\Licence $license
     */
    public function removeLicense(\AppBundle\Entity\Licence $license)
    {
        $this->licenses->removeElement($license);
    }

    /**
     * Get licenses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicenses()
    {
        return $this->licenses;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Licence
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Licence
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add lead
     *
     * @param \AppBundle\Entity\Lead $lead
     *
     * @return Licence
     */
    public function addLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \AppBundle\Entity\Lead $lead
     */
    public function removeLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads->removeElement($lead);
    }

    /**
     * Get leads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeads()
    {
        return $this->leads;
    }
}
