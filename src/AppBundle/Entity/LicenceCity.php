<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity
 * @ORM\Table(name="licence_city")
 */
class LicenceCity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Licence", inversedBy="licenses")
     */
    private $licence;

    /**
     * @ManyToOne(targetEntity="City", inversedBy="cities")
     */
    private $city;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $maxLimit;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="licenceCities")
     */
    private $user;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Lead", mappedBy="licence")
     */
    protected $leads;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->leads = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set maxLimit
     *
     * @param integer $maxLimit
     *
     * @return LicenceCity
     */
    public function setMaxLimit($maxLimit)
    {
        $this->maxLimit = $maxLimit;

        return $this;
    }

    /**
     * Get maxLimit
     *
     * @return integer
     */
    public function getMaxLimit()
    {
        return $this->maxLimit;
    }

    /**
     * Set licence
     *
     * @param \AppBundle\Entity\Licence $licence
     *
     * @return LicenceCity
     */
    public function setLicence(\AppBundle\Entity\Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return \AppBundle\Entity\Licence
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return LicenceCity
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return LicenceCity
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add lead
     *
     * @param \AppBundle\Entity\Lead $lead
     *
     * @return LicenceCity
     */
    public function addLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \AppBundle\Entity\Lead $lead
     */
    public function removeLead(\AppBundle\Entity\Lead $lead)
    {
        $this->leads->removeElement($lead);
    }

    /**
     * Get leads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeads()
    {
        return $this->leads;
    }
}
