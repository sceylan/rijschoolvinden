<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{


    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVATING = 1;
    const STATUS_ACTIVATED = 2;
    const STATUS_DEACTIVATED = 3;

    const WIZARD_NOT_COMPLETED = 0;
    const WIZARD_COMPLETED = 1;

    const WELCOME_NOT_COMPLETED = 0;
    const WELCOME_COMPLETED = 1;

    const REGISTRATION_REMINDED_FALSE = 0;
    const REGISTRATION_REMINDED_TRUE = 1;

    const SUBSCRIPTION_LEGACY = 0;
    const SUBSCRIPTION_QUARTERLY = 1;
    const SUBSCRIPTION_YEARLY = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $companyName;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $houseNumber;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $zipcode;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $registrationNumber;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(type="integer")
     */
    protected $registrationDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $subscriptionType;

    /**
     * @ORM\Column(type="integer")
     */
    protected $lastUpdatedDate;

    /**
     * @ORM\Column(type="integer")
     */
    protected $wizardCompleted;

    /**
     * @ORM\Column(type="integer")
     */
    protected $welcomeCompleted;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $registrationReminded;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\LicenceCity", mappedBy="user")
     */
    protected $licenceCities;

    /**
     * @OneToMany(targetEntity="UserLead", mappedBy="user")
     */
    protected $leads;

    /**
     * @ORM\OneToMany(targetEntity="Setting", mappedBy="user")
     */
    protected $setting;



    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return User
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set houseNumber
     *
     * @param string $houseNumber
     *
     * @return User
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber
     *
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return User
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set registrationNumber
     *
     * @param string $registrationNumber
     *
     * @return User
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get registrationNumber
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set registrationDate
     *
     * @param integer $registrationDate
     *
     * @return User
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

    /**
     * Get registrationDate
     *
     * @return integer
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * Set lastUpdatedDate
     *
     * @param integer $lastUpdatedDate
     *
     * @return User
     */
    public function setLastUpdatedDate($lastUpdatedDate)
    {
        $this->lastUpdatedDate = $lastUpdatedDate;

        return $this;
    }

    /**
     * Get lastUpdatedDate
     *
     * @return integer
     */
    public function getLastUpdatedDate()
    {
        return $this->lastUpdatedDate;
    }

    /**
     * Set wizardCompleted
     *
     * @param integer $wizardCompleted
     *
     * @return User
     */
    public function setWizardCompleted($wizardCompleted)
    {
        $this->wizardCompleted = $wizardCompleted;

        return $this;
    }

    /**
     * Get wizardCompleted
     *
     * @return integer
     */
    public function getWizardCompleted()
    {
        return $this->wizardCompleted;
    }

    /**
     * Set welcomeCompleted
     *
     * @param integer $welcomeCompleted
     *
     * @return User
     */
    public function setWelcomeCompleted($welcomeCompleted)
    {
        $this->welcomeCompleted = $welcomeCompleted;

        return $this;
    }

    /**
     * Get welcomeCompleted
     *
     * @return integer
     */
    public function getWelcomeCompleted()
    {
        return $this->welcomeCompleted;
    }

    /**
     * Add licenceCity
     *
     * @param \AppBundle\Entity\LicenceCity $licenceCity
     *
     * @return User
     */
    public function addLicenceCity(\AppBundle\Entity\LicenceCity $licenceCity)
    {
        $this->licenceCities[] = $licenceCity;

        return $this;
    }

    /**
     * Remove licenceCity
     *
     * @param \AppBundle\Entity\LicenceCity $licenceCity
     */
    public function removeLicenceCity(\AppBundle\Entity\LicenceCity $licenceCity)
    {
        $this->licenceCities->removeElement($licenceCity);
    }

    /**
     * Get licenceCities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicenceCities()
    {
        return $this->licenceCities;
    }

    /**
     * Add lead
     *
     * @param \AppBundle\Entity\UserLead $lead
     *
     * @return User
     */
    public function addLead(\AppBundle\Entity\UserLead $lead)
    {
        $this->leads[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \AppBundle\Entity\UserLead $lead
     */
    public function removeLead(\AppBundle\Entity\UserLead $lead)
    {
        $this->leads->removeElement($lead);
    }

    /**
     * Get leads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeads()
    {
        return $this->leads;
    }

    /**
     * Add setting
     *
     * @param \AppBundle\Entity\Setting $setting
     *
     * @return User
     */
    public function addSetting(\AppBundle\Entity\Setting $setting)
    {
        $this->setting[] = $setting;

        return $this;
    }

    /**
     * Remove setting
     *
     * @param \AppBundle\Entity\Setting $setting
     */
    public function removeSetting(\AppBundle\Entity\Setting $setting)
    {
        $this->setting->removeElement($setting);
    }

    /**
     * Get setting
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * Set registrationReminded
     *
     * @param integer $registrationReminded
     *
     * @return User
     */
    public function setRegistrationReminded($registrationReminded)
    {
        $this->registrationReminded = $registrationReminded;

        return $this;
    }

    /**
     * Get registrationReminded
     *
     * @return integer
     */
    public function getRegistrationReminded()
    {
        return $this->registrationReminded;
    }

    /**
     * Set subscriptionType
     *
     * @param integer $subscriptionType
     *
     * @return User
     */
    public function setSubscriptionType($subscriptionType)
    {
        $this->subscriptionType = $subscriptionType;

        return $this;
    }

    /**
     * Get subscriptionType
     *
     * @return integer
     */
    public function getSubscriptionType()
    {
        return $this->subscriptionType;
    }
}
