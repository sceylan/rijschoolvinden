<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_lead")
 */
class UserLead
{

    const STATUS_VALID = 0;
    const STATUS_REPORTED = 1;
    const STATUS_INVALID = 2;

    const STUDENT_UNKNOWN = 0;
    const STUDENT_NO = 1;
    const STUDENT_YES = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\Lead", inversedBy="leads")
     */
    private $lead;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="leads")
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\City", inversedBy="userLead")
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $dateTimeEmailed;

    /**
     * @ORM\Column(type="integer")
     */
    private $reported;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $reportReason;

    /**
     * @ORM\Column(type="integer")
     */
    protected $student;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lead
     *
     * @param \AppBundle\Entity\Lead $lead
     *
     * @return userLead
     */
    public function setLead(\AppBundle\Entity\Lead $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return \AppBundle\Entity\Lead
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return userLead
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return UserLead
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dateTimeEmailed
     *
     * @param integer $dateTimeEmailed
     *
     * @return UserLead
     */
    public function setDateTimeEmailed($dateTimeEmailed)
    {
        $this->dateTimeEmailed = $dateTimeEmailed;

        return $this;
    }

    /**
     * Get dateTimeEmailed
     *
     * @return integer
     */
    public function getDateTimeEmailed()
    {
        return $this->dateTimeEmailed;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return UserLead
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set reported
     *
     * @param integer $reported
     *
     * @return UserLead
     */
    public function setReported($reported)
    {
        $this->reported = $reported;

        return $this;
    }

    /**
     * Get reported
     *
     * @return integer
     */
    public function getReported()
    {
        return $this->reported;
    }

    /**
     * Set student
     *
     * @param integer $student
     *
     * @return UserLead
     */
    public function setStudent($student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return integer
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set reportReason
     *
     * @param string $reportReason
     *
     * @return UserLead
     */
    public function setReportReason($reportReason)
    {
        $this->reportReason = $reportReason;

        return $this;
    }

    /**
     * Get reportReason
     *
     * @return string
     */
    public function getReportReason()
    {
        return $this->reportReason;
    }
}
