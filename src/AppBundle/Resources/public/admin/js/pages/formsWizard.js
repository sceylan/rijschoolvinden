/*
 *  Document   : formsWizard.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Wizard page
 */

var FormsWizard = function() {

    return {
        init: function() {
            /*
             *  Jquery Wizard, Check out more examples and documentation at http://www.thecodemine.org
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Progress Wizard */
            // $('#progress-wizard').formwizard({focusFirstInput: true, disableUIStyles: true, inDuration: 0, outDuration: 0});

            // // Get the progress bar and change its width when a step is shown
            // var progressBar = $('#progress-bar-wizard');
            // progressBar
            //     .css('width', '33%')
            //     .attr('aria-valuenow', '33');


            //
            // $("#clickable-wizard").bind('step_shown', function(event, data){
            //     console.log(data.currentStep);
		     //    if (data.currentStep === 'progress-first') {
            //         progressBar
            //             .css('width', '50%')
            //             .attr('aria-valuenow', '50')
            //             .removeClass('progress-bar-warning progress-bar-success')
            //             .addClass('progress-bar-danger');
            //     }
            //     else if (data.currentStep === 'progress-second') {
            //         progressBar
            //             .css('width', '50%')
            //             .attr('aria-valuenow', '100')
            //             .removeClass('progress-bar-danger progress-bar-success')
            //             .addClass('progress-bar-success');
            //     }
                // else if (data.currentStep === 'progress-third') {
                //     progressBar
                //         .css('width', '100%')
                //         .attr('aria-valuenow', '100')
                //         .removeClass('progress-bar-danger progress-bar-warning')
                //         .addClass('progress-bar-success');
                // }
            // });

            /* Initialize Basic Wizard */
            // $('#basic-wizard').formwizard({disableUIStyles: true, inDuration: 0, outDuration: 0});

            /* Initialize Advanced Wizard with Validation */
            $('#clickable-wizard').formwizard({
                disableUIStyles: true,
                validationEnabled: true,
                validationOptions: {
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'span',
                    errorPlacement: function(error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function(e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function(e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        val_username: {
                            required: true,
                            minlength: 2
                        },
                        val_password: {
                            required: true,
                            minlength: 5
                        },
                        val_confirm_password: {
                            required: true,
                            equalTo: '#val_password'
                        },
                        val_email: {
                            required: true,
                            email: true
                        },
                        val_terms: {
                            required: true
                        }
                    },
                    messages: {
                        required: {
                            required: 'Please enter a asdasds',
                            minlength: 'Your username must consist of at least 2 characters'
                        },
                        val_username: {
                            required: 'Please enter a username',
                            minlength: 'Your username must consist of at least 2 characters'
                        },
                        val_password: {
                            required: 'Please provide a password',
                            minlength: 'Your password must be at least 5 characters long'
                        },
                        val_confirm_password: {
                            required: 'Please provide a password',
                            minlength: 'Your password must be at least 5 characters long',
                            equalTo: 'Please enter the same password as above'
                        },
                        val_email: 'Please enter a valid email address',
                        val_terms: 'Please accept the terms to continue'
                    }
                },
                inDuration: 0,
                outDuration: 0
            });

            jQuery.extend(jQuery.validator.messages, {
                required: "Dit veld is verplicht.",
                remote: "Please fix this field.",
                email: "Please enter a valid email address.",
                url: "Please enter a valid URL.",
                date: "Please enter a valid date.",
                dateISO: "Please enter a valid date (ISO).",
                number: "Please enter a valid number.",
                digits: "Please enter only digits.",
                creditcard: "Please enter a valid credit card number.",
                equalTo: "Please enter the same value again.",
                accept: "Please enter a value with a valid extension.",
                maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                minlength: jQuery.validator.format("Please enter at least {0} characters."),
                rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
                range: jQuery.validator.format("Please enter a value between {0} and {1}."),
                max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
                min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
            });

            /* Initialize Clickable Wizard */
            // var clickableWizard = $('#clickable-wizard');

            // clickableWizard.formwizard({disableUIStyles: true, inDuration: 0, outDuration: 0});

            $('.clickable-steps a').on('click', function(){
                var gotostep = $(this).data('gotostep');
                console.log(gotostep);

                clickableWizard.formwizard('show', gotostep);
            });

            // $('#submitBtn').click(function () {
            //     var stepInfo = $('#myform').formwizard('state');
            //     for (var i = 0; i < stepInfo.activatedSteps.length; i++) {
            //         stepInfo.steps.filter("#" + stepInfo.activatedSteps[i]).find(":input").not(".wizard-ignore").removeAttr("disabled");
            //     }
            // });
        }
    };
}();