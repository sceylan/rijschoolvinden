function openStep(step) {
    $('#question-step-'+step).slideDown();
}

function closeStep(step) {
    $('#question-step-'+step).slideUp();
}

function setValue(step, value) {
    closeStep(step);
    $('#label-step-'+step+' span').html(value);
    $('#label-step-'+step+' .fa-check').addClass('success');
    openStep(step+1);
    $('#label-step-'+step+'').fadeIn();

    // $(document.body).animate({
    //     'scrollTop':   $('#step'+step).offset().top - 200
    // }, 2000);
}

function closeAllSteps()
{
    var steps = [1, 2, 3, 4]
    for (i = 0; i < steps.length; i++) {
        $('#question-step-'+steps[i]).slideUp();
    }
}

function editStep(step) {
    closeAllSteps();
    $('#question-step-'+step).slideDown();
    // $(document.body).animate({
    //     'scrollTop':   $('#step'+step).offset().top - 200
    // }, 2000);
}

$(document).ready(function() {
    // Preloader Website
    // $('#loader-wrapper').delay(450).fadeOut();
    // $('#loader').delay(750).fadeOut('slow');

    $("#leadForm").validate({
        rules: {
            voornaam: {
                required: true
            },
            achternaam: {
                required: true
            },
            email: {
                required: true
            },
            telefoon: {
                required: true
            },

        },
        errorPlacement: function(error, element) { }
    });

    $('#start-configurator').click(function(){
        $(document.body).animate({
            'scrollTop':   $('#configurator').offset().top - 100
        }, 1000);
    });

    $('.step1-category').click(function(){
        var id = this.getAttribute('data-id');
        var type = this.getAttribute("data-type");
        $('#licenseID').val(id);
        setValue(1, type);
    });

    $('.step2').change(function() {
        var city = $('option:selected',this).text();
        var cityID = $(this).val();
        $('#cityID').val(cityID);
        setValue(2, city);
    });



});