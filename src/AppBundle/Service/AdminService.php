<?php

namespace AppBundle\Service;


use AppBundle\Entity\City;
use AppBundle\Entity\Licence;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCity;
use AppBundle\Entity\UserLead;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Lead;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService
{

    protected $em;
    protected $mailer;
    protected $templating;
    protected $validator;

    public function __construct(EntityManager $em, ContainerInterface $container, ValidatorService $validatorService)
    {
        $this->em = $em;
        $this->mailer = $container->get('mailer');
        $this->templating = $container->get('templating');
        $this->validator = $validatorService;
    }

    public function remindCompanies()
    {

        $users = $this->em->getRepository('AppBundle:User')->findBy(['welcomeCompleted' => User::WELCOME_NOT_COMPLETED]);

        foreach($users as $user)
        {
            $registrationDate = date('Y-m-d', $user->getRegistrationDate());
            $registrationDateWeekAfter = date('Y-m-d', strtotime($registrationDate . "+7 days"));
            $today = date('Y-m-d');

            // if todays date is greather then registrationdate + 7 days, it means that the registrationdate is older then 10 days.
            // email ucstomer with reminder please complete your account.
            if($today > $registrationDateWeekAfter)
            {
                // todo fix email template with reminder
                $message = \Swift_Message::newInstance()
                    ->setSubject('Uw registratie bij Rijschoolvinden')
                    ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setTo([$user->getEmail() => $user->getCompanyName()])
                    ->setBody(
                        $this->templating->render(
                            'AppBundle:Emails:lead.html.twig',
                            array(
                                'user' => $user
                            )
                        ),
                        'text/html'
                    );

                $user->setRegistrationReminded(User::REGISTRATION_REMINDED_TRUE);
                $this->em->flush();

            }



        }

//        $user = $this->em->getRepository('AppBundle:User')->find($userID);
//


    }
}