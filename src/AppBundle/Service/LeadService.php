<?php

namespace AppBundle\Service;


use AppBundle\Entity\City;
use AppBundle\Entity\Licence;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCity;
use AppBundle\Entity\UserLead;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Lead;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LeadService
{

    protected $em;
    protected $mailer;
    protected $templating;
    protected $validator;

    public function __construct(EntityManager $em, ContainerInterface $container, ValidatorService $validatorService)
    {
        $this->em = $em;
        $this->mailer = $container->get('mailer');
        $this->templating = $container->get('templating');
        $this->validator = $validatorService;
    }

    public function sendLeads()
    {

//        $message = \Swift_Message::newInstance()
//            ->setSubject('Cronjob executed')
//            ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
//            ->setTo(['info@puresoftware.nl' => 'Sinan Ceylan'])
//            ->setBody('Cron Executed!');
//        $this->mailer->send($message);
//
        $leads = $this->em->getRepository('AppBundle:Lead')->findBy(['status' => Lead::STATUS_NEW]);
        foreach($leads as $lead)
        {

            $insertedDate = date('Y-m-d', $lead->getInserted());
            $leadDate = date('Y-m-d', strtotime($insertedDate . "+7 days"));
            $today = date('Y-m-d');

            // if todays date is greather then leaddate + 7 days, it means that the lead is older then 7 days.
            // Update the lead status to old lead and e-mail customer that his/her lead is older and will not be emailed again.
            if($today > $leadDate)
            {
                $lead->setStatus(Lead::STATUS_EXPIRED);
                $this->em->flush();

                $message = \Swift_Message::newInstance()
                    ->setSubject('Aanvraag verlopen')
                    ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setTo([$lead->getEmailAddress() => $lead->getFirstName() . " " . $lead->getLastName()])
                    ->setBody(
                        $this->templating->render(
                            'AppBundle:Emails:verlopen.html.twig',
                            array(
                                'lead' => $lead,
                            )
                        ),
                        'text/html'
                    );

                $this->mailer->send($message);
                continue;
            }


            $city = $lead->getCity();
            $licence = $lead->getLicence();
            if(!$city || !$licence)
                throw new \Exception("Leadservice exception: City or Licence not found with leadID: " . $lead->getId() . "!");

            // List of user id's that are active in the lead city
            $userIDS = $this->activeInCityWithLicence($city, $licence);

            if(!$userIDS)
                continue;

            // If there are more then 5 users, this function will check and return 5 userID's max;
            $usersToBeEmailed = $this->retrieveFiveUsersToBeEmailed($userIDS, $city, $licence);

            // Start e-mailing the leads to users
            foreach($usersToBeEmailed as $userID)
            {

                $user = $this->em->getRepository('AppBundle:User')->find($userID);

                $message = \Swift_Message::newInstance()
                    ->setSubject('Nieuwe offerteaanvraag')
                    ->setFrom(['info@rijschoolvinden.nl' => 'Rijschoolvinden.nl'])
                    ->setTo([$user->getEmail() => $user->getCompanyName()])
                    ->setBody(
                        $this->templating->render(
                            'AppBundle:Emails:lead.html.twig',
                            array(
                                'lead' => $lead,
                                'companyName' => $user->getCompanyName()
                                )
                        ),
                        'text/html'
                    );

                $userLead = new UserLead();
                $userLead->setLead($lead);
                $userLead->setUser($user);
                $userLead->setCity($lead->getCity());
                $userLead->setDateTimeEmailed(time());
                $userLead->setReported(UserLead::STATUS_VALID);
                $userLead->setReportReason(null);
                $userLead->setStudent(UserLead::STUDENT_UNKNOWN);
                $this->em->persist($userLead);

                $lead->setStatus(Lead::STATUS_EMAILED);
                $this->em->flush();

                $this->mailer->send($message);

            }

        }

    }

    private function activeInCityWithLicence($cityObject, $licenceObject)
    {
        $userIDS = [];
        $userLicenceCity = $this->em->getRepository('AppBundle:LicenceCity')->findBy(['city' => $cityObject, 'licence' => $licenceObject]);
        if($userLicenceCity) {
            foreach ($userLicenceCity as $licenceCity)
            {
                $user = $licenceCity->getUser();
                if($user->getStatus() !== User::STATUS_ACTIVATED)
                    continue;

                $userID = $user->getId();
                $monthlyLimit = $licenceCity->getMaxLimit();
                $validUserID = $this->checkCityLicenceUserMonthlyLimit($userID, $cityObject, $licenceObject, $monthlyLimit);

                if($this->validator->validateNumericValue($validUserID))
                    $userIDS[] = $validUserID;
            }
        } else {
            return false;
        }
        return $userIDS;
    }

    /**
     * Function checks all monthly treshold per user and removes it from the list and returns cleaned list of user ids
     * @param $userIDS
     * return $userID
     */
    private function checkCityLicenceUserMonthlyLimit($userID, $city, $licence, $monthlyMaxLimit)
    {
        return $userID;

        if($monthlyMaxLimit === NULL)


        $lastMonth = strtotime(date('Y-m-01'));
        $qb = $this->em->createQueryBuilder();

        $result = $qb->select('COUNT(ul)')
            ->from('AppBundle:UserLead', 'ul')
            ->innerJoin('ul.lead', 'l')
            ->where('ul.user = :id')
            ->andWhere('ul.dateTimeEmailed > :dateTimeEmailed')
            ->andWhere('l.city = :city')
            ->andWhere('l.licence = :licence')
            ->setParameter('id', $userID)
            ->setParameter('dateTimeEmailed', $lastMonth)
            ->setParameter('city', $city)
            ->setParameter('licence', $licence)
            ->getQuery()
            ->getScalarResult();

        $count = array_map('current', $result);
        $finalCount = (int)$count[0];


        if($finalCount < $monthlyMaxLimit)
            return $userID;

        return false;

    }

    /**
     * If there are more then 5 userids to be emailed, this function retrieves all send leads based on datetime emailed, city and licence type.
     * The five non latest users will be returned and those user id's will be emailed the lead.
     * @param $userIDS
     */
    private function retrieveFiveUsersToBeEmailed($userIDS, $city, $licence)
    {
        if(count($userIDS) > 5)
        {
            /**
             * Check if user received email in the past
             */
            foreach($userIDS as $userID)
            {
                $leads = $this->em->getRepository('AppBundle:UserLead')->findBy(['user' => $userID]);
                if(!$leads)
                    $finalUserIdList[] = $userID;
            }


            $finalUserIdList = [];
            $qb = $this->em->createQueryBuilder();

            $result = $qb->select('ul')
                ->from('AppBundle:UserLead', 'ul')
                ->innerJoin('ul.lead', 'l')
                ->where('l.city = :city')
                ->andWhere('l.licence = :licence')
                ->andWhere('ul.user IN (:userIDS)')
                ->groupBy('ul.user')
                ->orderBy('ul.dateTimeEmailed', 'ASC')
                ->setMaxResults(5)
                ->setParameter('city', $city)
                ->setParameter('licence', $licence)
                ->setParameter('userIDS', $userIDS)
                ->getQuery()
                ->getResult();

            foreach($result as $userLead)
            {
                if(count($finalUserIdList) >= 5)
                    continue;

                $finalUserIdList[] = $userLead->getUser()->getId();
            }

            return $finalUserIdList;

        } else {
            return $userIDS;
        }
    }

}