<?php

namespace AppBundle\Service;


class ValidatorService
{

    public function __construct()
    {
    }

    /**
     * Validates e-mail address with php filter and checks if the domainname exists.
     * @param $email
     * @return bool
     */
    public function validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return false;

        if(!$this->validateDomainName($email))
            return false;

        return true;
    }

    /**
     * Validates Dutch Phone numbers.
     * @param $phoneNumber
     * @return bool
     */
    public function validatePhoneNumber($phoneNumber)
    {
        if(!preg_match('~(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)~', $phoneNumber))
            return false;

        return true;
    }

    /**
     * Checks if the domain name exists in order to validate an email address.
     * @param $email
     * @param string $record
     * @return bool
     */
    public function validateDomainName($email, $record = 'MX')
    {
        $domain = explode('@', $email);
        return checkdnsrr($domain[1], $record);
    }

    /**
     * Validates empty values, must have at least 1 characters or else will be defined as empty.
     * @param $value
     * @return bool
     */
    public function validateEmptyValue($value)
    {
        if(!isset($value))
            return false;

        if($value === null)
            return false;

        if(strlen($value) < 1)
            return false;

        return true;
    }

    /**
     * Validates all numeric values - infinite, must be at least 1
     * @param $value
     * @return bool
     */
    public function validateNumericValue($value)
    {
        if(!is_numeric($value))
            return false;

        if(!preg_match('~^[0-9]+$~', $value))
            return false;

        return true;
    }

    /**
     * Validates Dutch Zipcodes
     * @param $value
     * @return bool
     */
    public function validateZipCode($value)
    {
        if(!preg_match('~^([0-9]{4})([A-Z]{2})$~i', $value))
            return false;

        return true;
    }

    public function validatePasswordComplexity($password)
    {
        if(!preg_match('~^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,40})~', $password))
            return false;

        return true;
    }

}